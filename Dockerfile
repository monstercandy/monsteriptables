FROM monstercommon

ADD lib /opt/MonsterIptables/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterGeoip/lib/bin/iptables-install.sh && /opt/MonsterIptables/lib/bin/iptables-test.sh

ENTRYPOINT ["/opt/MonsterIptables/lib/bin/iptables-start.sh"]
