var lib = module.exports = function(app) {

    var router = app.ExpressPromiseRouter()

    var banLib = require("lib-bans.js")

    var bans = banLib(app)


    router.route("/save")
      .post(function(req,res,next){
         return req.sendOk(bans.SaveState())
      })

    router.route("/:category/:ip")
      .delete(function(req,res,next){
         return req.sendOk(
            bans.Release(req.params.category, req.params.ip)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "ban_release", e_other: req.params})
              })
         )
      })

    router.route("/:category")
      .delete(function(req,res,next){
          return req.sendOk(
            bans.FlushCategory(req.params.category)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "ban_release", e_other: req.params})
              })
          )
      })
      .put(function(req,res,next){
         return req.sendOk(
             bans.Insert(req.params.category, req.body.json)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "ban_insert", e_other: req.params})
              })
         )
      })
      .get(function(req,res,next){
         return req.sendResponse(bans.List(req.params.category))
      })

    router.route("/")
      .get(function(req,res,next){
         return req.sendResponse(bans.List())
      })


    return router


}
