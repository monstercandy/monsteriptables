var staticLib = module.exports = function(app) {

    const MError = app.MError
    const lockFile = require("MonsterLockfile")
    const dotqLib = require("MonsterDotq");
    const fs = dotqLib.fs()
    const path = require("path")
    const stateFilePath = path.join(app.config.get("volume_directory"), "bans.json.state");

	const moment = require("MonsterMoment")

	var common_params = app.config.get("common_params")

	var instanceLib = {}

    var number_of_changes = 0;

    if(!app.currentState)
	   app.currentState = {}
	app.config.get("ban_categories").forEach(c=>{
	    if(!app.currentState[c])
		   app.currentState[c] = {}
	})

    var vali = require("MonsterValidators").ValidateJs()
    vali.validators.notWhitelisted = function(value, options, key, attributes, glob) {
      if(!value) return
      if(app.config.get("whitelisted_ips").indexOf(value) > -1) return "whitelisted ip"
    }


    instanceLib.SaveState = function(){
    	return save_logic()
    }

    instanceLib.Release = function(category, ip){
    	validateCategory(category)
    	if(!app.currentState[category][ip])
    		throw new MError("IP_NOT_BLOCKED")

    	var cmd = app.config.get("cmd_release")
    	var d = {category: category, ip: ip}
    	console.log("Releasing banned IP", category, ip)
		return app.commander.spawn({executeImmediately: true, removeImmediately:true, chain:cmd}, [common_params,d])
		  .then(h=>{
		  	  return h.executionPromise
		  })
		  .then(()=>{
		  	 delete app.currentState[category][ip];
             number_of_changes++;
		  })
    }

    instanceLib.FlushCategory = function(category){
    	validateCategory(category)

        var cmd = app.config.get("cmd_flush_category")
        console.log("Flushing category", category)
		return app.commander.spawn({executeImmediately: true, removeImmediately:true, chain:cmd}, [common_params, {category: category}])
		  .then(h=>{
		  	  return h.executionPromise
		  })
		  .then(()=>{
		  	 app.currentState[category] = {}
             number_of_changes++;
		  })

    }
    instanceLib.List = function(category){
    	if(!category)
    		return app.currentState
    	validateCategory(category)
    	return app.currentState[category]
    }

    function validateCategory(category){
    	if(!app.currentState[category])
    		throw new MError("INVALID_CATEGORY")
    	return category
    }

	instanceLib.Insert = function(category, in_data, stateParameters){
		in_data.category = category
		var d
		return vali.async(in_data, {
			category: {presence: true, inclusion: app.config.get("ban_categories")},
			// note: isValidIPv4 will prevent internal ips being added
			ip: {presence: true, isValidIPv4: true, notWhitelisted: true,},
			ban_duration: {isInteger: true, default: app.config.get("default_ban_duration_seconds")},
		})
		.then((ad)=>{
			d = ad
			if(!app.currentState[d.category][d.ip]) {
				// we need to insert the iptables rule first
				var cmd = app.config.get("cmd_ban")
				console.log("Adding new ban", d)
				return app.commander.spawn({executeImmediately: true, removeImmediately:true, chain:cmd}, [common_params,d])
				  .then(h=>{
				  	  return h.executionPromise
				  })
			} else {
				console.log("Extending ban", d)
     			return Promise.resolve()
			}
		})
		.then(()=>{
			app.currentState[d.category][d.ip] =  stateParameters || {ban_duration: d.ban_duration, added: moment.nowUnixtime()}
            number_of_changes++;
		})
	}

	install_release_and_save_logic()

    return instanceLib

    function install_release_and_save_logic(){
    	if(!app.release_logic_installed) {
    		app.release_logic_installed = true
    		setInterval(release_logic, app.config.get("release_interval_seconds")*1000)
    	}
    	if(!app.save_logic_installed) {
    		app.save_logic_installed = true
    		setInterval(function(){
                if(!number_of_changes) return;
                number_of_changes = 0;

                save_logic();
            }, app.config.get("save_interval_seconds")*1000)

    		init_logic()
    	}
    }

    function release_logic(){
    	var now = moment.nowUnixtime()
        return dotqLib.linearMap({array:Object.keys(app.currentState), action: function(category){
            return dotqLib.linearMap({array:Object.keys(app.currentState[category]), catch: true, action: function(ip){
                var v = app.currentState[category][ip];
                var expires = v.added + v.ban_duration;
                if(expires < now)
                    return instanceLib.Release(category, ip);

            }})
        }})

    }

    function save_logic(){
        return lockFile.logic({lockfile: "iptables", params:{wait:5000}, callback: function(){
        	console.log("Saving state to", stateFilePath)
        	return fs.writeFileAsync(stateFilePath, JSON.stringify(app.currentState))
        }})

    }

    function init_logic(){
    	return fs.readFileAsync(stateFilePath, "utf8")
    	  .then(content=>{
    	  	  console.log("Restoring from state file", stateFilePath)
    	  	  var d = JSON.parse(content)
    	  	  return dotqLib.linearMap({array:Object.keys(d), action:function(category){
                return instanceLib.FlushCategory(category)
                  .then(()=>{
                      return dotqLib.linearMap({array:Object.keys(d[category]), action: function(ip){
                         return instanceLib.Insert(category, {ip: ip}, d[category][ip])
                      }});
                  })
              }});
    	  })
    	  .catch(ex=>{
    	  	 if(ex.code == "ENOENT") return // state file was not present, omitting error

    	  	 console.error("Unable to recover from state file", ex)
    	  })
    }

}
