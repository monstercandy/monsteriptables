var lib = module.exports = function(app) {

   const whExtraLib = require("lib-whextra.js")
   var whExtra = whExtraLib(app);

   var wie = {};

   const dotq = require("MonsterDotq");

   wie.BackupWh = function(wh, req) {
      return whExtra.GetFirewallRules(wh.wh_id)
   }

   wie.GetAllWebhostingIds = function(){
      return whExtra.GetAllWebhostingIds();
   }

   wie.RestoreWh = function(wh, in_data, req) {
      var oldData;
      return whExtra.GetFirewallRules(wh.wh_id)
        .then((aOldData)=>{
           oldData = aOldData;
           return whExtra.saveStuffAsIs(wh.wh_id, in_data)
        })
        .then(()=>{
           return whExtra.regenerateFirewallForWh(wh.wh_id, in_data, oldData.active != in_data.active)
        })
        .then(()=>{
           app.InsertEvent(req, {e_event_type: "restore-wh-iptables", e_other: wh.wh_id})

        })

   }



   return wie;

}
