var staticLib = module.exports = function(app) {

    const MError = app.MError
	var common_params = app.config.get("common_params")

    const lockFile = require("MonsterLockfile")
    const dotq = require("MonsterDotq");
    const fs = dotq.fs()
    const path = require("path")

	var instanceLib = {}

    var valiLib = require("MonsterValidators");
    var vali = valiLib.ValidateJs();


    var firewallLoggingCache = {};

     function compareRules(a,b){
        var match = true;
        Array("fw_dst_ip", "fw_dst_port", "fw_action").some(function(c) {
           if(a[c] != b[c]) {
             match = false;
             return true;
           }
        })
        return match;
     }

    instanceLib.DelFirewallRule = function(webhosting_id, in_data) {
       var params = validateWhId(webhosting_id);
       var d;
       return validateInsertDelPayload(in_data)
         .then(ad=>{
             d = ad;
             return readFirewallRules(params)
         })
         .then(data=>{

            var matches = 0;
            data.rules.forEach((item)=>{
                if(!compareRules(item, d)) return;

                matches++;
                var index = data.rules.indexOf(item);
                data.rules.splice(index, 1);
            });

            if(!matches)
              throw new MError("RULE_NOT_FOUND");

            return saveFirewallRules(params, data);
         })
         .then(data=>{
            if(!data.active) return Promise.resolve();

            return regenerateFirewall(params, data);
         })


    }

    instanceLib.StartFirewallLogging = function(webhosting_id) {

      var logfile = app.config.get("iptables_logfile")
      if(!logfile)
          return Promise.reject(new MError("FEATURE_NOT_ENABLED"))

      var params = validateWhId(webhosting_id);
      if(firewallLoggingCache[params.wh_id])
          return Promise.reject(new MError("ALREADY_LOGGING"))

      var max_time = app.config.get("firewall_logging_interval_ms")

      var gemitter

      return readFirewallRules(params)
        .then(data=>{
           if(!data.active) throw new MError("FIREWALL_NOT_ENABLED")

           firewallLoggingCache[params.wh_id] = true

           return regenerateFirewall(params);
        })
        .then(()=>{

            gemitter = app.commander.EventEmitter()
            return gemitter.spawn({
                omitAggregatedOutput:true,
                max_execution_time_ms: max_time,
                logToError:true
            })
        })
        .then(h=>{

            setTimeout(function(){
               gemitter.send_stdout_ln("You should see the firewall events here in real time.");
            },3000)

            var tail = new app.Tail(logfile);
            tail.on("line", function(data) {
                if(data.indexOf(params.fw_chain_name) < 0) return

                gemitter.send_stdout(data)
            });

            tail.on("error", function(error) {
                console.error('tail: ', error);
                gemitter.close(1)
            });


           h.executionPromise
              .catch(ex=>{})
              .then(()=>{
                 delete firewallLoggingCache[params.wh_id]
                 regenerateFirewall(params)
                 tail.unwatch()
              })

           return Promise.resolve(h)
        })

    }

    instanceLib.GetFirewallRules = function(webhosting_id) {
       var params = validateWhId(webhosting_id);
       return readFirewallRules(params);
    }
    instanceLib.AddFirewallRule = function(webhosting_id, in_data) {
       var params = validateWhId(webhosting_id);
       var d;
       var webhosting;
       return validateInsertDelPayload(in_data)
         .then(ad=>{
            d = ad
            return app.MonsterInfoWebhosting.GetInfo(params.wh_id)
         })
         .then(awh=>{
            webhosting = awh;
            return readFirewallRules(params);
         })
         .then((data)=>{

             /*
             dupe checking logic
             */
             console.log("shit", data, "X", d);
             var alreadyThere = false;
             data.rules.some(function(row){
                if(compareRules(row, d)) {
                   alreadyThere = true;
                   return true;
                }
             })

             if(alreadyThere) {
              console.log("already there")
                return;
             }

             if(data.rules.length >= webhosting.template.t_max_number_of_firewall_rules)
                throw new MError("TOO_MANY_RULES")

             data.rules.push(d);
             return saveFirewallRules(params, data)
             .then(data=>{
                if(!data.active) return;

                return regenerateFirewall(params);
             })
         })
    }
    instanceLib.DeleteFirewallRules = function(webhosting_id) {
       var params = validateWhId(webhosting_id);
       return readFirewallRules(params)
         .then(data=>{
             if(data.active) return deleteFirewallChain(params)
             return Promise.resolve();
         })
         .then(()=>{
            return fs.ensureRemovedAsync(params.dataFilePath)
         })
    }

    instanceLib.SetStatus = function(webhosting_id, in_data) {
       var params = validateWhId(webhosting_id);
       var d;
       return vali.async(in_data, {active: {presence:true, isBooleanLazy: true}})
         .then(ad=>{
             d = ad;
             return readFirewallRules(params);
         })
         .then(data=>{
             if(d.active == data.active)
                return Promise.resolve();

             data.active = d.active;
             return saveFirewallRules(params, data)
               .then(()=>{
                  return (d.active ? firewallTurnOn(params) : firewallTurnOff(params));
               })
         })
    }

    instanceLib.GetAllWebhostingIds = function(){
        return fs.readdirAsync(app.config.get("whextra_store_directory_path"))
          .then(files=>{
              var r = new RegExp("([0-9]+)\.json");
              var toread = [];
              files.forEach(x=>{
                 var m = r.exec(x);
                 if(m) toread.push(m[1]);
              })

              return toread;
          })

    }

    instanceLib.GetAllData = function(){
        return instanceLib.GetAllWebhostingIds()
          .then(toread=>{

              var re = {};
              return dotq.linearMap({array:toread, catch:true, action:function(wh_id){
                 var params = validateWhId(wh_id);
                 return readFirewallRules(params)
                   .then(data=>{
                      re[wh_id] = extend({}, params, data);
                   })
              }})
              .then(()=>{
                 return re;
              })
          })

    }

    instanceLib.GetAllRules = function(){
      return instanceLib.GetAllData()
        .then(dataObjects=>{
           var re = [];
           Object.keys(dataObjects).forEach(wh_id=>{
              dataObjects[wh_id].rules.forEach(rule=>{
                  rule.webhosting_id = wh_id;
                  re.push(rule);
              })
           })

           return re;
        })
    }

    instanceLib.saveStuffAsIs = function(wh_id, data){
          var params = validateWhId(wh_id);
          return saveFirewallRules(params, data);
    }

    instanceLib.regenerateFirewallForWh = function(wh_id, data, force){
          if((!data.active)&&(!force)) return;

          var params = validateWhId(wh_id);
          return regenerateFirewall(params, data);
    }


    readAndRebuildAllFirewallChains();

    return instanceLib


    function readAndRebuildAllFirewallChains(){
      if(app.readAndRebuildAllFirewallChains) return;
      app.readAndRebuildAllFirewallChains = true;

       if(typeof afterEach == "function") {
          console.log("running as a mocha script, not regenerating any fw rules");
          return;
       }

      return instanceLib.GetAllData()
        .then(dataObjects=>{
            var keys = Object.keys(dataObjects);
            if(keys.length <= 0) return;

            console.log("regernerating firewall chains")

            return dotq.linearMap({array:keys, catch:true, action: function(wh_id){
                var data = dataObjects[wh_id];
                if(data.active)
                   return firewallTurnOn(data);
                return instanceLib.regenerateFirewallForWh(wh_id, data);
            }})
        })
    }


    function firewallTurnOff(params){
       return deleteFirewallChain(params)
    }
    function firewallTurnOn(params){
       return createFirewallChain(params)
         .then(()=>{
              return regenerateFirewall(params) //
         })
    }

    function validateInsertDelPayload(in_data){
      var constraints = {
               "fw_dst_ip": {presence: true, isValidIPv4: {canBeRange: true,rejectIpsFromRange:[],canBeZero:true}},
               "fw_dst_port": {presence: true, isInteger: {greaterThanOrEqualTo:1, lessThenOrEqualTo: 65535}, },
               "fw_action": {inclusion:["ACCEPT","REJECT"], default:"ACCEPT"},
            };
       return vali.async(in_data, constraints);
    }

    function inLock(params, callback){
       return lockFile.logic({lockfile: params.lockfileName, params:{wait:5000}, callback: callback})
    }

    function saveFirewallRules(params, data) {
       return inLock(params,function(){
          return fs.writeFileAsync(params.dataFilePath, JSON.stringify(data));
       })
       .then(()=>{
          return Promise.resolve(data)
       })
    }

    function readFirewallRules(params) {

       return inLock(params,function(){
         return fs.readFileAsync(params.dataFilePath, "utf-8")
           .then(data=>{
               return JSON.parse(data);
           })
           .catch(ex=>{
              if(ex.code == "ENOENT") return defaultContent();

              console.error("Content of", params.dataFilePath, "is broken");
           })
       })
    }

    function defaultContent(){
       return {active:false, rules:[]};
    }

    function deleteFirewallChainReferences(params){

        console.log("deleting firewall chain references for", params.wh_id)

       var cmd = app.config.get("cmd_delete_whextra_chain_refs");

       return new Promise(function(resolve,reject){

           repeatDelete();

           function repeatDelete(){
            app.commander.spawn({removeImmediately:true,executeImmediately:true,chain:cmd}, [common_params, params])
             .then((h)=>{
                return h.executionPromise
             })
             .then(()=>{
                // it was possible to delete, additional ones might exist
                repeatDelete()
             })
             .catch(ex=>{
                // if this fails, it means there are no remaining ones
                resolve()
             })
           }

        })

    }
    function createFirewallChain(params){

        console.log("creating firewall chain for", params.wh_id)

       var cmdNew = app.config.get("cmd_create_firewall_chain_new");
       var cmdRef = app.config.get("cmd_create_firewall_chain_ref");

       return app.commander.spawn({removeImmediately:true,executeImmediately:true,chain:cmdNew}, [common_params, params])
         .then(h=>{
            return h.executionPromise
         })
         .catch(ex=>{
            if(ex.output.match(/Chain already exists/)) {
               console.log("The chain alrady exists, proceeding");
               return Promise.resolve();
            }
            console.error("couldnt create chain", params, ex)
            throw ex;

         })
         .then(()=>{
            return deleteFirewallChainReferences(params)
         })
         .then(()=>{
              return app.commander.spawn({removeImmediately:true,executeImmediately:true,chain:cmdRef}, [common_params, params])
         })
         .then(h=>{
            return h.executionPromise
         })

    }
    function deleteFirewallChain(params){

       var cmdFlush = app.config.get("cmd_delete_firewall_chain_flush");
       var cmdDelete = app.config.get("cmd_delete_firewall_chain_delete");

        return deleteFirewallChainReferences(params)
           .then(()=>{
               console.log("deleting firewall chain for", params.wh_id)

               return app.commander.spawn({removeImmediately:true,executeImmediately:true,chain:cmdFlush}, [common_params, params])
           })
           .then(h=>{
              return h.executionPromise
           })
           .then(()=>{
               return app.commander.spawn({removeImmediately:true,executeImmediately:true,chain:cmdDelete}, [common_params, params])
           })
           .then(h=>{
              return h.executionPromise
           })

    }
    function regenerateFirewall(params, data){

       console.log("regernerating firewall chain for", params.wh_id)

       var p = data ? Promise.resolve(data) : readFirewallRules(params);

       return p.then(data=>{
          var rules = data.rules;

          var rulesStr = ""
          if(firewallLoggingCache[params.wh_id])
             rulesStr += `-A ${params.fw_chain_name} -j LOG --log-level 7 --log-prefix "[netfilter] ${params.fw_chain_name}: "\n`

          rules.forEach(rule=>{
             rulesStr += `-A ${params.fw_chain_name} -p tcp -d ${rule.fw_dst_ip} --dport ${rule.fw_dst_port} -j ${rule.fw_action}\n`
          })

          var stdin = `*filter\n-F ${params.fw_chain_name}\n${rulesStr}\n-A ${params.fw_chain_name} -j REJECT\nCOMMIT\n`

          var cmd = simpleCloneObject(app.config.get("cmd_iptables_restore"));
          cmd.stdin = stdin;

          return app.commander.spawn({
              executeImmediately:true,
              removeImmediately: true,
              chain:cmd,
          }, [common_params, params])
       })
       .then(h=>{
          return h.executionPromise
       })

    }


    function validateWhId(wh_id){
        if(!valiLib.IsNumericString(wh_id))  throw new MError("VALIDATION_ERROR", ["not an integer"]);
        var fn = path.join(app.config.get("whextra_store_directory_path"), wh_id+".json");
        var lockfileName = "iptables-whextra-"+wh_id;
        return {wh_id:wh_id, fw_chain_name: "WH"+wh_id, dataFilePath: fn, lockfileName: lockfileName};
    }

}
