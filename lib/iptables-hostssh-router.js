var lib = module.exports = function(app) {

    var router = app.ExpressPromiseRouter()

    router.post("/", function(req,res,next){
         return req.sendOk(
            vali.async(req.body.json, {
               ip: {presence: true, IsValidIPv4: true}
            })
            .then(d=>{
              return resetHostsshWhitelistChain(d.ip)
            })
            .then(()=>{
               app.InsertEvent(req, {e_event_type: "iptables-hostssh-ip-set"});
            })
         )
      })


    return router

  function resetHostsshWhitelistChain(ip){

          var chainName = "HOSTSSH_WHITELIST";
          var rulesStr = "-A ${chainName} -s ${ip} -j ACCEPT";

          var stdin = `*filter\n-F ${chainName}\n${rulesStr}\nCOMMIT\n`

          var cmd = simpleCloneObject(app.config.get("cmd_iptables_restore"));
          cmd.stdin = stdin;

          return app.commander.spawn({
              executeImmediately:true,
              removeImmediately: true,
              chain:cmd,
          }, [common_params, params])
         .then(h=>{
            return h.executionPromise
         })    
  }

}
