var lib = module.exports = function(app) {

    var router = app.ExpressPromiseRouter({mergeParams: true})

    var whExtraLib = require("lib-whextra.js")

    var whExtra = whExtraLib(app)


    router.delete("/firewallrules/rule", function (req, res, next) {
          return req.sendOk(
             whExtra.DelFirewallRule(req.params.webhosting_id, req.body.json)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "firewall_rule_remove", e_other: req.body.json})
              })
          )
      })

    router.post("/firewallrules/logging", function (req, res, next) {
          return req.sendTask(whExtra.StartFirewallLogging(req.params.webhosting_id))
    })

    router.route("/firewallrules/status")
      .post(function (req, res, next) {
          return req.sendOk(
             whExtra.SetStatus(req.params.webhosting_id, req.body.json)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "firewall_status_change", e_other: {p:req.params,d:req.body.json}})
              })
          )
      })

    router.route("/firewallrules/")
      .get(function (req, res, next) {
          return req.sendPromResultAsIs(whExtra.GetFirewallRules(req.params.webhosting_id))
      })
      .put(function (req, res, next) {
          return req.sendOk(
            whExtra.AddFirewallRule(req.params.webhosting_id, req.body.json)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "firewall_rule_insert", e_other: {p:req.params,d:req.body.json}})
              })
          )
      })
      .delete(function (req, res, next) {
          return req.sendOk(
             whExtra.DeleteFirewallRules(req.params.webhosting_id)
              .then(()=>{
                 app.InsertEvent(req, {e_event_type: "firewall_rule_remove", e_other: req.params})
              })
          )
      })

    return router
}
