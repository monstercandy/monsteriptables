#!/bin/bash

set -e

. /opt/MonsterCommon/lib/common.inc

mkdir -p "/var/lib/monster/$INSTANCE_NAME/whextra"
mkdir -p "/var/log/monster/$INSTANCE_NAME"
