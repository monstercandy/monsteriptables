//permissions needed: ["EMIT_LOG","INFO_WEBHOSTING"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "iptables"

    var fs = require("MonsterDotq").fs();

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    Array("whextra_store_directory_path").forEach(x=>{
      if(!config.get(x)) throw new Error("Mandatory parameter "+x+" missing")
    })

    config.defaults({
      "whitelisted_ips":[],
      "default_ban_duration_seconds": 60*60,
      "release_interval_seconds": 60,
      "save_interval_seconds": 600,
      "common_params":{
         "iptables_path": "/sbin/iptables",
         "iptables_restore_path": "/sbin/iptables-restore",
      },

      "firewall_logging_interval_ms": 900000,

      "ban_categories": ["HTTP", "EMAIL", "FTP"],
      "cmd_ban": {executable: "[iptables_path]", args: ["-A","[category]_BAN", "-s", "[ip]", "-j", "DROP"]},
      "cmd_release": {executable: "[iptables_path]", args: ["-D","[category]_BAN", "-s", "[ip]", "-j", "DROP"]},
      "cmd_flush_category": {executable: "[iptables_path]", args: ["-F","[category]_BAN"]},

      "cmd_delete_whextra_chain_refs": {executable: "[iptables_path]", args: ["-D", "WHEXTRA", "-m", "owner",  "--uid-owner", "[wh_id]", "-j", "[fw_chain_name]"]},
      "cmd_create_firewall_chain_new": {executable: "[iptables_path]", args: ["-N", "[fw_chain_name]"]},
      "cmd_create_firewall_chain_ref": {executable: "[iptables_path]", args: ["-A", "WHEXTRA", "-m", "owner",  "--uid-owner", "[wh_id]", "-j", "[fw_chain_name]"]},
      "cmd_delete_firewall_chain_flush": {executable: "[iptables_path]", args: ["-F", "[fw_chain_name]"]},
      "cmd_delete_firewall_chain_delete": {executable: "[iptables_path]", args: ["-X", "[fw_chain_name]"]},

      "cmd_iptables_restore": {executable: "[iptables_restore_path]", args: ["--noflush"]},

    })


    config.appRestPrefix = "/iptables"

    var app = me.Express(config, moduleOptions);
    app.MError = me.Error
    app.Tail = require('MonsterTail').Tail;

    const MonsterInfoLib = require("MonsterInfo")


    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var router = app.PromiseRouter(config.appRestPrefix)

    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})

    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])
    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)

    router.use("/hostssh", require("iptables-hostssh-router.js")(app))

    router.use("/bans", require("iptables-bans-router.js")(app))
    router.get("/whextra/", function(req,res,next){
       const whExtra = require("lib-whextra.js")(app);
       return req.sendPromResultAsIs(whExtra.GetAllRules());
    });
    router.use("/whextra/:webhosting_id", require("iptables-whextra-router.js")(app))

    require("Wie")(app, router, require("lib-wie.js")(app));

    app.Cleanup = function(){
       const del = require("MonsterDel")
       var patternsToDel = []
       patternsToDel.push(path.join(config.get("whextra_store_directory_path"), "*.json"))
       return del(patternsToDel,{"force": true})
         .then(()=>{
           console.log("sht just removed");
         })
    }

    return app
}
