require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../iptables-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


    var tokens = {}
    var userIds = {}

    var defaultPayload = {fw_dst_ip:"123.123.123.123",fw_dst_port: 1234}

    var storage_id = 31111;
    var getInfoCalls = 0;

    const expectedBackup = { active: true,
  rules:
   [ { fw_dst_ip: '123.123.123.123',
       fw_dst_port: 1234,
       fw_action: 'ACCEPT' } ] };

    app.MonsterInfoWebhosting = {
       GetInfo: function(id){
           assert.equal(id, storage_id);

           getInfoCalls++

           return Promise.resolve({
             wh_id: id,
             wh_user_id: 123,
             template:{
               t_max_number_of_firewall_rules:1,
             },
           })
       }
    }



	describe('basic tests', function() {


        it("adding a firewall rule without the firewall being turned on", function(done){

                 mapi.put( "/whextra/"+storage_id+"/firewallrules",defaultPayload,function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result,"ok")
                    done()

                 })

        })

    it("turning on the firewall on", function(done){

            var commands = 0
            app.commander = {
              spawn: function(c, params){
                  // console.log("spawn was called", c, params)
                  commands++;
                  if(commands == 2)
                    return Promise.resolve({id: commands, executionPromise: Promise.reject()}) // this will indicate that removing the rule has failed, so the iteration will stop
                 return Promise.resolve({id: commands, executionPromise: Promise.resolve()})
               }
            }

            mapi.post( "/whextra/"+storage_id+"/firewallrules/status", {active:true}, function(err, result, httpResponse){
               // console.log("!!!", err, result)
               assert.equal(result, "ok")
               done()
            })

    })


	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

      cleanup();

        it('firewall stuff should be empty', function(done) {

             mapi.get( "/whextra/"+storage_id+"/firewallrules",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, { active: false, rules: [] })
                done()

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })

        })
  }


  function cleanup(){
      it("cleanup first", function(done){
            var commands = 0;
            app.commander = {
              spawn: function(c, params){
                  commands++;
                  // console.log("spawn was called", c, params)
                  if(commands == 1)
                     return Promise.resolve({id: commands, executionPromise: Promise.reject()}) // this will indicate that removing the rule has failed, so the iteration will stop

                   return Promise.resolve({id: commands, executionPromise: Promise.resolve()})
               }
            }


             mapi.delete( "/whextra/"+storage_id+"/firewallrules/", {}, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })
      })
  }

  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

              console.log(result);

                latestBackup = simpleCloneObject(result);

                var actual = result;

                assert.deepEqual(actual, expectedBackup);
                done()

             })

        })
  }


}, 10000)

