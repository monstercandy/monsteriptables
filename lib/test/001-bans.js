require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../iptables-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

  const common_params = app.config.get("common_params")

  const expectedIp = "111.111.111.111"


	describe('basic ban tests', function() {


      shouldBeEmpty();


      Array("192.168.1.1","10.6.6.6","127.0.0.1","127.0.0.2").forEach(ip=>{
          shouldBeBlocked('subnetwork ip addresses should be rejected', ip);
      })
      Array("123.123.123.123").forEach(ip=>{
          shouldBeBlocked('whitelisted ip addresses should be rejected', ip);
      })

      shouldBeBlocked('invalid category should be rejected', "1.1.1.1", "FOOBAR")

      addingFirst();

      shouldReturnTheEntry();

      it("even when adding the same again, but this time without iptables execution", function(done){
            var commandExecutions = 0;
            var oCommander = app.commander
            app.commander = {
              spawn: function(cmd, params){
                  // console.log("cmd",cmd,"params", params)
                  commandExecutions++;
                  throw new Error("should not be here");
              }
            }
             mapi.put(
                "/bans/HTTP",
                {
                  ip: expectedIp,
                }, function(err, result, httpResponse){
                // console.log(err, result)
                 assert.equal(result, "ok");
                 assert.equal(commandExecutions, 0)
                 app.commander = oCommander;
                 done();
             });
      });

      shouldReturnTheEntry();


        it('removing should throw when not found', function(done) {

             mapi.delete(
                "/bans/HTTP/111.111.111.112",
                {}, function(err, result, httpResponse){
                // console.log(err, result)
                 assert.propertyVal(err, "message", "IP_NOT_BLOCKED");
                 done();
             });

        })


        it('removing it should be good', function(done) {

            var commandExecutions = 0;
            var oCommander = app.commander;
            app.commander = {
              spawn: function(cmd, params){
                   // console.log("cmd",cmd,"params", params)
                  commandExecutions++;
                  assert.deepEqual(cmd, { executeImmediately: true,
                    removeImmediately: true,
                    chain:
                     { executable: '[iptables_path]',
                       args: [ '-D', '[category]_BAN', '-s', '[ip]', '-j', 'DROP' ] } });
                  assert.deepEqual(params, [ common_params,
  { ip: expectedIp, category: 'HTTP' } ])

                  return Promise.resolve({id:"foo", executionPromise: Promise.resolve()})
              }
            }


             mapi.delete(
                "/bans/HTTP/"+expectedIp,
                {}, function(err, result, httpResponse){
                // console.log(err, result)
                 assert.equal(result, "ok");
                 assert.equal(commandExecutions, 1)
                 app.commander = oCommander;
                 done();
             });

        })



        shouldBeEmpty();


        addingFirst();


        it('removing a complete category', function(done) {

            var commandExecutions = 0;
            var oCommander = app.commander;
            app.commander = {
              spawn: function(cmd, params){
                  //  console.log("cmd",cmd,"params", params)
                  commandExecutions++;
                  assert.deepEqual(cmd, { executeImmediately: true,
                    removeImmediately: true,
                    chain:
                     { executable: '[iptables_path]',
                       args: [ '-F', '[category]_BAN' ] } });
                  assert.deepEqual(params, [ common_params, { category: 'HTTP' } ])

                  return Promise.resolve({id:"foo", executionPromise: Promise.resolve()})
              }
            }


             mapi.delete(
                "/bans/HTTP/",
                {}, function(err, result, httpResponse){
                // console.log(err, result)
                 assert.equal(result, "ok");
                 assert.equal(commandExecutions, 1)
                 app.commander = oCommander;
                 done();
             });

        })

        shouldBeEmpty();

    })

   function shouldBeBlocked(desc, ip, category) {
      if(!category) category = category

      it(desc+": "+ip, function(done){
             mapi.put(
                "/bans/"+category,
                {
                  ip: ip,
                }, function(err, result, httpResponse){
                // console.log(err, result)
                 assert.propertyVal(err, "message", "VALIDATION_ERROR")
                 done()
             })
      })
   }

   function shouldBeEmpty() {
       it("fetching a category of bans should be empty", function(done){
             mapi.get(
                "/bans/HTTP", function(err, result, httpResponse){
                // console.log(err, result)
                 assert.deepEqual(result, {})
                 done()
             })
       })

       it("fetching the root of bans should be empty", function(done){
             mapi.get(
                "/bans/", function(err, result, httpResponse){
                // console.log(err, result)
                app.config.get("ban_categories").forEach(c=>{
                  assert.deepEqual(result[c],{})
                  delete result[c]
                })
                 assert.deepEqual(result, {})
                 done()
             })
       })
   }

   function shouldReturnTheEntry() {
       it("fetching a category of bans should return the one", function(done){
             mapi.get(
                "/bans/HTTP", function(err, result, httpResponse){
                // console.log(err, result)
                 assert.property(result, expectedIp);
                 assert.property(result[expectedIp], "added");
                 assert.property(result[expectedIp], "ban_duration");
                 done();
             })
       })

       it("fetching the root of bans should return the one", function(done){
             mapi.get(
                "/bans/", function(err, result, httpResponse){
                // console.log(err, result)
                 assert.property(result.HTTP, expectedIp)
                 assert.property(result.HTTP[expectedIp], "added");
                 assert.property(result.HTTP[expectedIp], "ban_duration");
                 done()
             })
       })
   }

   function addingFirst(){

        it('otherwise we should be good with adding stuff', function(done) {

            var commandExecutions = 0;
            var oCommander = app.commander
            app.commander = {
              spawn: function(cmd, params){
                  // console.log("cmd",cmd,"params", params)
                  commandExecutions++;
                  assert.deepEqual(cmd, { executeImmediately: true,
                    removeImmediately: true,
                    chain:
                     { executable: '[iptables_path]',
                       args: [ '-A', '[category]_BAN', '-s', '[ip]', '-j', 'DROP' ] } });
                  assert.deepEqual(params, [ common_params,
  { ip: expectedIp, category: 'HTTP', ban_duration: 3600 } ])

                  return Promise.resolve({id:"foo", executionPromise: Promise.resolve()})
              }
            }


             mapi.put(
                "/bans/HTTP",
                {
                  ip: expectedIp,
                }, function(err, result, httpResponse){
                // console.log(err, result)
                 assert.equal(result, "ok");
                 assert.equal(commandExecutions, 1)
                 app.commander = oCommander;
                 done();
             });


        })
   }

}, 10000)


