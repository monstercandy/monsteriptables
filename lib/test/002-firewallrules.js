require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../iptables-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const expectedSpawnParameters = [ { iptables_path: '/sbin/iptables',
    iptables_restore_path: '/sbin/iptables-restore' },
  { wh_id: '11111',
    fw_chain_name: 'WH11111',
    dataFilePath: 'vol.d\\whextra\\11111.json',
    lockfileName: 'iptables-whextra-11111' } ];

    var tokens = {}
    var userIds = {}

    var defaultPayload = {fw_dst_ip:"123.123.123.123",fw_dst_port: 1234}

    var webhosting_id = 11111;
    var getInfoCalls = 0;

    app.MonsterInfoWebhosting = {
       GetInfo: function(id){
           assert.equal(id, webhosting_id);

           getInfoCalls++

           return Promise.resolve({
             wh_id: id,
             wh_user_id: 123,
             template:{
               t_max_number_of_firewall_rules:1,
             },
           })
       }
    }



describe("firewall rules", function(){

        shouldBeEmpty()


    it("adding a firewall rule with invalid syntax should be rejected", function(done){

             mapi.put( "/whextra/"+webhosting_id+"/firewallrules",{fw_dst_ip:"hello",fw_dst_port:1234},function(err, result, httpResponse){
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

    })


    addRuleShouldWork();

    addRuleShouldWork(); // adding it once again should silently be accepted

    it("should be rejected because of max number of rules reached", function(done){

             mapi.put( "/whextra/"+webhosting_id+"/firewallrules",{fw_dst_ip:"123.123.123.125",fw_dst_port: 1234},function(err, result, httpResponse){
                assert.propertyVal(err, "message", "TOO_MANY_RULES")
                done()

             })

    })

    shouldBeThere(false);

    it("removing a rule by content should work", function(done){

             mapi.delete( "/whextra/"+webhosting_id+"/firewallrules/rule",defaultPayload,function(err, result, httpResponse){
                assert.equal(result, "ok")
                done()
             })

    })

    it("removing a rule by invalid content should throw", function(done){

             mapi.delete( "/whextra/"+webhosting_id+"/firewallrules/rule",defaultPayload,function(err, result, httpResponse){
                assert.propertyVal(err, "message", "RULE_NOT_FOUND")
                done()
             })

    })

    addRuleShouldWork()



    it("logging should return error, since firewall is not started", function(done){
            mapi.post( "/whextra/"+webhosting_id+"/firewallrules/logging", {}, function(err, result, httpResponse){
               // console.log("!!!", err, result)
               assert.propertyVal(err, "message", "FIREWALL_NOT_ENABLED")
               done()
            })
    })


    it("turning on the firewall should generate everything", function(done){

            var commands = 0
            app.commander = {
              spawn: function(c, params){
                 commands++

                 // console.log("task:", commands, c, "params", params)

                 assert.deepEqual(params, expectedSpawnParameters)

                 if(commands == 1) {
                    assert.deepEqual(c, {removeImmediately: true, executeImmediately: true, chain: { executable: '[iptables_path]',
     args: [ '-N', '[fw_chain_name]' ] } })
                 }
                 if(commands == 2) {
                    assert.deepEqual(c, {removeImmediately: true,executeImmediately: true, chain: { executable: '[iptables_path]',
     args:
      [ '-D',
        'WHEXTRA',
        '-m',
        'owner',
        '--uid-owner',
        '[wh_id]',
        '-j',
        '[fw_chain_name]' ] }})

                    return Promise.resolve({id: commands, executionPromise: Promise.reject()}) // this will indicate that removing the rule has failed, so the iteration will stop
                 }
                 if(commands == 3) {
                    assert.deepEqual(c, {removeImmediately: true,executeImmediately: true, chain:
{ executable: '[iptables_path]',
     args:
      [ '-A',
        'WHEXTRA',
        '-m',
        'owner',
        '--uid-owner',
        '[wh_id]',
        '-j',
        '[fw_chain_name]' ] }
                       })
                 }
                 if(commands == 4) {
                    assert.deepEqual(c, {removeImmediately: true,executeImmediately: true,
                      chain:
                       { executable: '[iptables_restore_path]',
     args: [ '--noflush' ],
     stdin: '*filter\n-F WH11111\n-A WH11111 -p tcp -d 123.123.123.123 --dport 1234 -j ACCEPT\n\n-A WH11111 -j REJECT\nCOMMIT\n' } }
                    )

                 }

                 return Promise.resolve({id: commands, executionPromise: Promise.resolve()})
               }
            }

            mapi.post( "/whextra/"+webhosting_id+"/firewallrules/status", {active:true}, function(err, result, httpResponse){
               // console.log("!!!", err, result)
               assert.equal(result, "ok")
               assert.equal(commands, 4)
               done()
            })

    })


    shouldBeThere(true);

    it("logging should work now", function(done){
            var commands = 0
            var oTail = app.Tail
            var tailWasCalled = 0
            var unwatchWasCalled = 0
            const readerId = 123

            app.Tail = function(logfile){
               assert.equal(logfile, app.config.get("iptables_logfile"))
               tailWasCalled++
               return {
                  "on": function(cat, cb){
                  },
                  unwatch: function(){
                     unwatchWasCalled++
                  }
               }
            }
            app.commander = {
              EventEmitter: function() {
                  return {
                     spawn: function(cb) {
                         // console.log("via eventemitter", cb)
                         assert.deepEqual(cb, { omitAggregatedOutput: true,max_execution_time_ms: 900000, logToError: true})
                         return Promise.resolve({id:readerId,executionPromise: Promise.resolve()})
                     }
                  }
              },
              spawn: function(c, params){
                 console.log("spawn:", c)
                 commands++
                 assert.deepEqual(params, expectedSpawnParameters)

                 if(commands == 1) {
                    assert.deepEqual(c, { removeImmediately: true,
  executeImmediately: true,
  chain:
   { executable: '[iptables_restore_path]',
     args: [ '--noflush' ],
     stdin: '*filter\n-F WH11111\n-A WH11111 -j LOG --log-level 7 --log-prefix "[netfilter] WH11111: "\n-A WH11111 -p tcp -d 123.123.123.123 --dport 1234 -j ACCEPT\n\n-A WH11111 -j REJECT\nCOMMIT\n' } })

                 }
                 if(commands == 2){
                    assert.deepEqual(c, { removeImmediately: true,
  executeImmediately: true,
  chain:
   { executable: '[iptables_restore_path]',
     args: [ '--noflush' ],
     stdin: '*filter\n-F WH11111\n-A WH11111 -p tcp -d 123.123.123.123 --dport 1234 -j ACCEPT\n\n-A WH11111 -j REJECT\nCOMMIT\n' } })

                    app.Tail = oTail
                    assert.equal(tailWasCalled, 1)
                    assert.equal(unwatchWasCalled, 1)
                    done()
                 }

                 return Promise.resolve({id: commands, executionPromise: Promise.resolve()})
               }

            }


            mapi.post( "/whextra/"+webhosting_id+"/firewallrules/logging", {}, function(err, result, httpResponse){
               // console.log("!!!", err, result)
               assert.isNull(err)
               assert.propertyVal(result, "id", readerId)
            })
    })


    it("turning off the firewall should invoke iptables to remove them", function(done){

           var commands = 0
            app.commander = {
              spawn: function(c, params){
                 commands++
                 console.log("spawn:", commands, c)

                 assert.deepEqual(params, expectedSpawnParameters)

                 if(commands == 1) {
                    assert.deepEqual(c, { removeImmediately: true, executeImmediately: true, chain:
{ executable: '[iptables_path]',
     args:
      [ '-D',
        'WHEXTRA',
        '-m',
        'owner',
        '--uid-owner',
        '[wh_id]',
        '-j',
        '[fw_chain_name]' ] } })

                    return Promise.resolve({id: commands, executionPromise: Promise.reject()})
                 }
                 if(commands == 2) {
                   assert.deepEqual(c, { removeImmediately:true,executeImmediately: true, chain:

{ executable: '[iptables_path]',
     args: [ '-F', '[fw_chain_name]' ] } })
                 }
                 if(commands == 3) {
                   assert.deepEqual(c, { removeImmediately:true,executeImmediately: true, chain:
{ executable: '[iptables_path]',
     args: [ '-X', '[fw_chain_name]' ] } })
                 }

                 return Promise.resolve({id: commands, executionPromise: Promise.resolve()})
               }
            }

            mapi.post( "/whextra/"+webhosting_id+"/firewallrules/status", {active:false}, function(err, result, httpResponse){
               // console.log("!!!", err, result)
               assert.equal(result, "ok")
               assert.equal(commands, 3)
               done()
            })

    })


    shouldBeThere(false);

       it("should be listed in top level as well", function(done){

             mapi.get( "/whextra/",function(err, result, httpResponse){
                assert.ok(Array.isArray(result));
                assert.equal(result.length, 1);
                var r = result[0]
                assert.deepEqual(r, {
                   "fw_action": "ACCEPT",
                   "fw_dst_ip": "123.123.123.123",
                   "fw_dst_port": 1234,
                   "webhosting_id": ""+webhosting_id,
                 })
                done()
             })

       })

      it("deleting rules at root webhosting level should work", function(done){
            mapi.delete( "/whextra/"+webhosting_id+"/firewallrules", {}, function(err, result, httpResponse){
               // console.log("!!!", err, result)
               assert.equal(result, "ok")
               done()

            })

      })

      shouldBeEmpty()


     function shouldBeEmpty(){
        it("firewall ruleset should be empty", function(done){

                 mapi.get( "/whextra/"+webhosting_id+"/firewallrules",function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.property(result, "active")
                    assert.deepEqual(result.rules, [])
                    done()

                 })

        })

     }

    function addRuleShouldWork(){

        it("adding a firewall rule without the firewall being turned on", function(done){

                 mapi.put( "/whextra/"+webhosting_id+"/firewallrules",defaultPayload,function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(result,"ok")
                    done()

                 })

        })

    }

    function shouldBeThere(active){

       it("should be there and as active: "+active, function(done){

             mapi.get( "/whextra/"+webhosting_id+"/firewallrules",function(err, result, httpResponse){
                assert.deepEqual(result, {active:active, rules: [{
            "fw_action": "ACCEPT",
             "fw_dst_ip": "123.123.123.123",
             "fw_dst_port": 1234,
           }]})
                done()
             })

       })
    }


})




}, 10000)

